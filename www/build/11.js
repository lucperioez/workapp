webpackJsonp([11],{

/***/ 710:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyTeamPageModule", function() { return MyTeamPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_team__ = __webpack_require__(727);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MyTeamPageModule = /** @class */ (function () {
    function MyTeamPageModule() {
    }
    MyTeamPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__my_team__["a" /* MyTeamPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__my_team__["a" /* MyTeamPage */]),
            ],
        })
    ], MyTeamPageModule);
    return MyTeamPageModule;
}());

//# sourceMappingURL=my-team.module.js.map

/***/ }),

/***/ 727:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyTeamPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the MyTeamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyTeamPage = /** @class */ (function () {
    function MyTeamPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    MyTeamPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MyTeamPage');
    };
    MyTeamPage.prototype.detail = function () {
        this.navCtrl.push('EmployeeDetailPage');
    };
    MyTeamPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-my-team',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/my-team/my-team.html"*/'<ion-content padding>\n\n    <ion-list>\n\n        <ion-item>\n          <ion-thumbnail item-start>\n            <img class="avatar" src="../assets/img/marty-avatar.png">\n          </ion-thumbnail>\n          <h2>Jonathan Silva</h2>\n          <p>Tablaroca, Estructuras</p>\n          <button ion-button clear (click)="detail()" item-end>Details</button>\n        </ion-item>\n    \n        <ion-item>\n          <ion-thumbnail item-start>\n            <img class="avatar" src="../assets/img/marty-avatar.png">\n          </ion-thumbnail>\n          <h2>Raiders of the Lost Ark</h2>\n          <p>Steven Spielberg • 1981</p>\n          <button ion-button clear item-end>View</button>\n        </ion-item>\n    \n        <ion-item>\n          <ion-thumbnail item-start>\n            <img class="avatar" src="../assets/img/marty-avatar.png">\n          </ion-thumbnail>\n          <h2>Ghostbusters</h2>\n          <p>Ivan Reitman • 1984</p>\n          <button ion-button clear item-end>View</button>\n        </ion-item>\n    \n        <ion-item>\n          <ion-thumbnail item-start>\n            <img class="avatar" src="../assets/img/marty-avatar.png">\n          </ion-thumbnail>\n          <h2>Batman</h2>\n          <p>Tim Burton • 1988</p>\n          <button ion-button clear item-end>View</button>\n        </ion-item>\n    \n        <ion-item>\n          <ion-thumbnail item-start>\n            <img class="avatar" src="../assets/img/marty-avatar.png">\n          </ion-thumbnail>\n          <h2>Back to the Future</h2>\n          <p>Robert Zemeckis • 1985</p>\n          <button ion-button clear item-end>View</button>\n        </ion-item>\n    \n        <ion-item>\n          <ion-thumbnail item-start>\n            <img class="avatar" src="../assets/img/marty-avatar.png">\n          </ion-thumbnail>\n          <h2>The Empire Strikes Back</h2>\n          <p>Irvin Kershner • 1980</p>\n          <button ion-button clear item-end>View</button>\n        </ion-item>\n    \n        <ion-item>\n          <ion-thumbnail item-start>\n            <img class="avatar" src="../assets/img/marty-avatar.png">\n          </ion-thumbnail>\n          <h2>The Terminator</h2>\n          <p>James Cameron • 1984</p>\n          <button ion-button clear item-end>View</button>\n        </ion-item>\n    \n      </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/my-team/my-team.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], MyTeamPage);
    return MyTeamPage;
}());

//# sourceMappingURL=my-team.js.map

/***/ })

});
//# sourceMappingURL=11.js.map