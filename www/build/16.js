webpackJsonp([16],{

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardsPageModule", function() { return CardsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cards__ = __webpack_require__(722);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CardsPageModule = /** @class */ (function () {
    function CardsPageModule() {
    }
    CardsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__cards__["a" /* CardsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__cards__["a" /* CardsPage */]),
                __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__cards__["a" /* CardsPage */]
            ]
        })
    ], CardsPageModule);
    return CardsPageModule;
}());

//# sourceMappingURL=cards.module.js.map

/***/ }),

/***/ 722:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers__ = __webpack_require__(91);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CardsPage = /** @class */ (function () {
    function CardsPage(navCtrl, employee, events) {
        this.navCtrl = navCtrl;
        this.employee = employee;
        this.events = events;
        events.publish('update:menu', 'Search results');
    }
    CardsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.employee.getEmployees().subscribe(function (resp) {
            console.log(resp);
            _this.Employees = resp['users'];
        });
    };
    CardsPage.prototype.detail = function (id) {
        this.navCtrl.push('EmployeeDetailPage', { id: id });
    };
    CardsPage.prototype.references = function () {
        this.navCtrl.push('ReferencesPage');
    };
    CardsPage.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.Title = page.title;
        this.navCtrl.setRoot(page.component);
    };
    CardsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cards',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/cards/cards.html"*/'  <ion-content class="workapp-content">\n \n\n      <!--<ion-card>\n        <ion-item>\n          <ion-row>\n            <ion-col col-3>\n              <img class="avatar" src="../assets/img/marty-avatar.png">\n            </ion-col>\n            <ion-col col-9 class="avatar-name">\n                <h2>Jonathan Silva</h2>\n                <p>Constructor, Electricista</p>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n        <ion-card-content>\n          <ion-row>\n            <ion-col col-3>\n              <b>Skill</b>\n            </ion-col>\n            <ion-col col-9 class="rank-stars">\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n            </ion-col>\n            <ion-col col-3>\n              <b>Puntuality</b>\n            </ion-col>\n            <ion-col col-9 class="rank-stars">\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n            </ion-col>\n            <ion-col col-3>\n              <b>Dicipline</b>\n            </ion-col>\n            <ion-col col-9 class="rank-stars">\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n            </ion-col>\n          </ion-row>\n          <ion-col col-12>\n            <b class="description-label">Description:</b>\n          </ion-col>\n          <ion-col col-12>\n            <p>\n              Soy un constructor de mucha experiencia \n              y me he trabajado en varios proyectos, \n              contratando gran numero de personas.\n            </p>\n          </ion-col>\n        </ion-card-content>\n          <ion-row class="butons-container">\n            <ion-col col-9>\n              <button ion-button outline class="btn-referencias" (click)="references(1)"  small icon-paper>\n                <ion-icon name="md-paper"></ion-icon>\n                References\n              </button>\n            </ion-col>\n            <ion-col col-3>\n              <button ion-button outline class="btn-detalle" (click)="detail(1)"  small icon-start> \n                <ion-icon  name="md-person"></ion-icon>\n                Detail\n              </button>\n            </ion-col>\n        \n          </ion-row>\n      </ion-card>-->\n      <ion-card *ngFor="let Employee of Employees; let i = index">\n        <ion-item>\n          <ion-row>\n            <ion-col col-3>\n              <img class="avatar" src="../assets/img/marty-avatar.png">\n            </ion-col>\n            <ion-col col-9 class="avatar-name">\n                <h2>{{Employee.user}}</h2>\n                <p>Constructor, Electricista</p>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n        <ion-card-content>\n          <ion-row>\n            <ion-col col-3>\n              <b>Skill</b>\n            </ion-col>\n            <ion-col col-9 class="rank-stars">\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n            </ion-col>\n            <ion-col col-3>\n              <b>Puntuality</b>\n            </ion-col>\n            <ion-col col-9 class="rank-stars">\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n            </ion-col>\n            <ion-col col-3>\n              <b>Dicipline</b>\n            </ion-col>\n            <ion-col col-9 class="rank-stars">\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n                <ion-icon name="md-star"></ion-icon>\n            </ion-col>\n          </ion-row>\n          <ion-col col-12>\n            <b class="description-label">Description:</b>\n          </ion-col>\n          <ion-col col-12>\n            <p>\n              Soy un constructor de mucha experiencia \n              y me he trabajado en varios proyectos, \n              contratando gran numero de personas.\n            </p>\n          </ion-col>\n        </ion-card-content>\n          <ion-row class="butons-container">\n            <ion-col col-9>\n              <button ion-button outline class="btn-referencias" (click)="references(1)"  small icon-paper>\n                <ion-icon name="md-paper"></ion-icon>\n                References\n              </button>\n            </ion-col>\n            <ion-col col-3>\n              <button ion-button outline class="btn-detalle" (click)="detail(Employee.id_user)"  small icon-start> \n                <ion-icon  name="md-person"></ion-icon>\n                Detail\n              </button>\n            </ion-col>\n        \n          </ion-row>\n      </ion-card>\n  \n  </ion-content>\n\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/cards/cards.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__providers__["c" /* Employee */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers__["c" /* Employee */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _c || Object])
    ], CardsPage);
    return CardsPage;
    var _a, _b, _c;
}());

//# sourceMappingURL=cards.js.map

/***/ })

});
//# sourceMappingURL=16.js.map