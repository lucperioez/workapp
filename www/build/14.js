webpackJsonp([14],{

/***/ 707:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeDetailPageModule", function() { return EmployeeDetailPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__employee_detail__ = __webpack_require__(724);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EmployeeDetailPageModule = /** @class */ (function () {
    function EmployeeDetailPageModule() {
    }
    EmployeeDetailPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__employee_detail__["a" /* EmployeeDetailPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__employee_detail__["a" /* EmployeeDetailPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__employee_detail__["a" /* EmployeeDetailPage */]
            ]
        })
    ], EmployeeDetailPageModule);
    return EmployeeDetailPageModule;
}());

//# sourceMappingURL=employee-detail.module.js.map

/***/ }),

/***/ 724:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeeDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers__ = __webpack_require__(91);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the EmployeeDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EmployeeDetailPage = /** @class */ (function () {
    function EmployeeDetailPage(navCtrl, navParams, alertCtrl, employee) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.employee = employee;
    }
    EmployeeDetailPage.prototype.ionViewDidLoad = function () {
        this.employee.getEmployeeDetail(this.navParams.get('id')).subscribe(function (resp) {
            console.log(resp);
        });
    };
    EmployeeDetailPage.prototype.ngOnInit = function () {
        this.date = new Date();
        this.reservedDays = {
            "initDate": "2018-09-01",
            "finishDate": "2018-11-15"
        };
        this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        this.getDaysOfMonth();
    };
    EmployeeDetailPage.prototype.getDaysOfMonth = function () {
        this.daysInThisMonth = new Array();
        this.daysInLastMonth = new Array();
        this.daysInNextMonth = new Array();
        this.currentMonth = this.monthNames[this.date.getMonth()];
        this.currentYear = this.date.getFullYear();
        if (this.date.getMonth() === new Date().getMonth()) {
            this.currentDate = new Date().getDate();
            console.log("Current");
            console.log(this.currentDate);
        }
        else {
            this.currentDate = 999;
        }
        var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
        var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
        for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
            this.daysInLastMonth.push({
                "NumberDay": i,
                "Date": this.formatDate(new Date(this.date.getFullYear(), this.date.getMonth() - 1, i))
            });
        }
        var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
        for (var i = 0; i < thisNumOfDays; i++) {
            this.daysInThisMonth.push({
                "NumberDay": i + 1,
                "Date": this.formatDate(new Date(this.date.getFullYear(), this.date.getMonth(), i + 1))
            });
        }
        var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
        var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
        for (var i = 0; i < (6 - lastDayThisMonth); i++) {
            this.daysInNextMonth.push({
                "NumberDay": i + 1,
                "Date": this.formatDate(new Date(this.date.getFullYear(), this.date.getMonth() + 1, i + 1))
            });
        }
        var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
        if (totalDays < 36) {
            for (var i = (7 - lastDayThisMonth); i < ((7 - lastDayThisMonth) + 7); i++) {
                this.daysInNextMonth.push({
                    "NumberDay": i + 1,
                    "Date": this.formatDate(new Date(this.date.getFullYear(), this.date.getMonth() + 1, i + 1))
                });
            }
        }
    };
    EmployeeDetailPage.prototype.formatDate = function (date) {
        var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    };
    EmployeeDetailPage.prototype.goToLastMonth = function () {
        this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
        console.log(this.date);
        this.getDaysOfMonth();
    };
    EmployeeDetailPage.prototype.goToNextMonth = function () {
        this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
        console.log(this.date);
        this.getDaysOfMonth();
    };
    EmployeeDetailPage.prototype.contact = function () {
        var confirm = this.alertCtrl.create({
            title: 'Sure you want to contact?',
            message: 'We will send a notification to the employee',
            buttons: [
                {
                    text: 'Disagree',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        console.log('Agree clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    EmployeeDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-employee-detail',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/employee-detail/employee-detail.html"*/'\n\n<ion-content  class="workapp-content" >\n  <ion-card>\n    <ion-item>\n      <ion-row>\n        <ion-col col-3>\n          <img class="avatar" src="../assets/img/marty-avatar.png">\n        </ion-col>\n        <ion-col col-9 class="avatar-name">\n            <h2>Jonathan Silva</h2>\n            <p>Constructor, Electricista</p>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <ion-card-content>\n      <ion-row>\n        <ion-col col-3>\n          <b class="rank-title">Skill</b>\n        </ion-col>\n        <ion-col col-9 class="rank-stars">\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n        </ion-col>\n        <ion-col col-3>\n          <b class="rank-title">Puntuality</b>\n        </ion-col>\n        <ion-col col-9 class="rank-stars">\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n        </ion-col>\n        <ion-col col-3>\n          <b class="rank-title">Dicipline</b>\n        </ion-col>\n        <ion-col col-9 class="rank-stars">\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n            <ion-icon name="md-star"></ion-icon>\n        </ion-col>\n      </ion-row>\n      <ion-col col-12>\n        <b class="description-label">Description:</b>\n      </ion-col>\n      <ion-col col-12>\n        <p class="employee-description">\n          Soy un constructor de mucha experiencia \n          y me he trabajado en varios proyectos, \n          contratando gran numero de personas.\n        </p>\n      </ion-col>\n      <ion-row>\n        <ion-col  col-3>\n        </ion-col>\n        <ion-col class="colorDescription"  col-2>\n        </ion-col>\n\n        <ion-col class="description-title" col-7>\n          Dias ocupados\n        </ion-col>\n      </ion-row>\n        <div >\n          <div class="description-calendar" text-center>\n            My calendar\n          </div>\n          <div class="calendar-header" >\n            <ion-row class="calendar-month">\n              <ion-col col-2 (click)="goToLastMonth()"><ion-icon name="arrow-back"></ion-icon></ion-col>\n              <ion-col col-8>{{currentMonth}} {{currentYear}}</ion-col>\n              <ion-col col-2 (click)="goToNextMonth()"><ion-icon name="arrow-forward"></ion-icon></ion-col>\n            </ion-row>\n          </div>\n          <div class="calendar-body" >\n            <ion-grid>\n              <ion-row class="calendar-weekday">\n                <ion-col>Su</ion-col>\n                <ion-col>Mo</ion-col>\n                <ion-col>Tu</ion-col>\n                <ion-col>We</ion-col>\n                <ion-col>Th</ion-col>\n                <ion-col>Fr</ion-col>\n                <ion-col>Sa</ion-col>\n              </ion-row>\n              <ion-row class="calendar-date">\n                <ion-col col-1 *ngFor="let lastDay of daysInLastMonth" class="last-month">\n                  <div class="otherMonthDate" *ngIf="(lastDay.Date >= reservedDays.initDate && lastDay.Date <= reservedDays.finishDate); else otherDate">{{lastDay.NumberDay}}</div>\n                  <ng-template #otherDate ><div class="otherDate">{{lastDay.NumberDay}}</div></ng-template>\n                </ion-col>\n                <ion-col col-1 *ngFor="let day of daysInThisMonth">\n                  <div class="reservedDate" *ngIf="(day.Date >= reservedDays.initDate && day.Date <= reservedDays.finishDate); else otherDate">{{day.NumberDay}}</div>\n                  <ng-template #otherDate class="otherDate"><div class="otherDate">{{day.NumberDay}}</div></ng-template>\n                </ion-col>\n                <ion-col col-1 *ngFor="let nextDay of daysInNextMonth" class="next-month">\n                  <div class="otherMonthDate" *ngIf="(nextDay.Date >= reservedDays.initDate && nextDay.Date <= reservedDays.finishDate); else otherDate">{{nextDay.NumberDay}}</div>\n                  <ng-template #otherDate class="otherDate"><div class="otherDate">{{nextDay.NumberDay}}</div></ng-template>\n                  \n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </div>\n        </div>\n        <ion-row class="butons-container">\n          <ion-col col-8>\n            <button ion-button outline class="btn-referencias" small icon-paper>\n              <ion-icon name="md-paper"></ion-icon>\n              Add to team\n            </button>\n          </ion-col>\n          <ion-col col-4>\n            <button ion-button outline class="btn-detalle"  (click)="contact()"  small icon-start> \n              <ion-icon  name="md-person"></ion-icon>\n              Contact\n            </button>\n          </ion-col>\n      \n        </ion-row>\n    </ion-card-content>\n      \n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/employee-detail/employee-detail.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__providers__["c" /* Employee */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers__["c" /* Employee */]) === "function" && _d || Object])
    ], EmployeeDetailPage);
    return EmployeeDetailPage;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=employee-detail.js.map

/***/ })

});
//# sourceMappingURL=14.js.map