webpackJsonp([15],{

/***/ 706:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigPageModule", function() { return ConfigPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config__ = __webpack_require__(723);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConfigPageModule = /** @class */ (function () {
    function ConfigPageModule() {
    }
    ConfigPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__config__["a" /* ConfigPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__config__["a" /* ConfigPage */]),
            ],
        })
    ], ConfigPageModule);
    return ConfigPageModule;
}());

//# sourceMappingURL=config.module.js.map

/***/ }),

/***/ 723:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_crop__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers__ = __webpack_require__(91);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ConfigPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ConfigPage = /** @class */ (function () {
    function ConfigPage(crop, loadingCtrl, navCtrl, navParams, storage, camera, formBuilder, user) {
        var _this = this;
        this.crop = crop;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.camera = camera;
        this.formBuilder = formBuilder;
        this.user = user;
        this.storage.get('usr').then(function (val) {
            _this.imprimir(val);
        });
        this.authForm = this.formBuilder.group({
            first_name: ['',],
            last_name: ['',],
            work_type: ['',],
            user: ['',],
            confirmPassword: ['',],
            cities: ['',],
            phone: ['',],
            description: ['',],
            experience: ['',]
        });
    }
    ConfigPage.prototype.imprimir = function (val) {
        var _this = this;
        this.user_detail = val.user_detail;
        if (this.user_detail.image != '') {
            this.imgPreview = this.user_detail.image;
        }
        else {
            this.imgPreview = 'assets/imgs/logo.png';
        }
        this.user_session = val.user;
        this.authForm.controls['first_name'].setValue(this.user_detail.first_name);
        this.authForm.controls['last_name'].setValue(this.user_detail.last_name);
        this.authForm.controls['work_type'].setValue(this.user_detail.work_type);
        this.authForm.controls['phone'].setValue(this.user_detail.phone);
        this.authForm.controls['user'].setValue(this.user_session.user);
        this.authForm.controls['cities'].setValue(this.user_detail.cities);
        this.authForm.controls['description'].setValue(this.user_detail.description);
        this.authForm.controls['experience'].setValue(this.user_detail.experience);
        this.user.getCities(this.user_detail.id_detail).subscribe(function (resp) {
            _this.user_cities = resp['employee_city'];
            var new_cities = _this.user_cities.map(function (c) {
                return c.city_id;
            });
            _this.authForm.controls['cities'].setValue(new_cities);
        }, function (err) {
        });
    };
    ConfigPage.prototype.getPhoto = function () {
        var _this = this;
        var options = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 100,
            targetWidth: 150,
            targetHeight: 150,
            encodingType: this.camera.EncodingType.PNG,
        };
        this.camera.getPicture(options).then(function (imageData) {
            return imageData;
        }).then(function (image) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            var base64Image = 'data:image/jpeg;base64,' + image;
            _this.imgPreview = base64Image;
        }, function (err) {
            // Handle error
        });
    };
    ConfigPage.prototype.update_info = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: "<p><img src=\"assets/img/loader.gif\" width=\"50px\" padding-horizontal /><br>\n                  Registrando datos espere un momento porfavor ...</p>"
        });
        loading.onDidDismiss(function () {
            _this.navCtrl.setRoot('ConfigPage');
        });
        var userData = this.authForm.value;
        userData.image = this.imgPreview;
        this.user.signup_employee(userData, this.user_session.id_user).subscribe(function (resp) {
            _this.storage.set('usr', { user: _this.user_session, user_detail: resp['user_detail'] });
            _this.user_detail = resp['user_detail'];
            console.log(_this.user_detail);
            loading.dismiss();
        }, function (err) {
        });
        if (this.authForm.value.cities != undefined) {
            var cities = this.authForm.value.cities.map(function (c) {
                return {
                    city_id: c
                };
            });
            this.user_cities.forEach(function (element) {
                _this.user.deleteCities(element.id_employee_city).subscribe(function (resp) {
                }, function (err) {
                });
            });
            cities.forEach(function (element) {
                _this.user.cities(_this.user_detail.id_detail, element).subscribe(function (resp) {
                    _this.user.getCities(_this.user_detail.id_detail).subscribe(function (resp) {
                        _this.user_cities = resp['employee_city'];
                        var new_cities = _this.user_cities.map(function (c) {
                            return c.city_id;
                        });
                        _this.authForm.controls['cities'].setValue(new_cities);
                    }, function (err) {
                    });
                }, function (err) {
                });
            });
            loading.present();
        }
    };
    ConfigPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-config',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/config/config.html"*/'\n<ion-content padding>\n    <form (ngSubmit)="update_info()" [formGroup]="authForm" novalidate>\n        <ion-grid class="register-content">\n          <ion-row>\n            <ion-col col-3>\n            </ion-col>\n            <ion-col col-6>\n              <ion-label fixed></ion-label>\n              <ion-avatar item-end>\n                <img src="{{imgPreview}}" (click)="getPhoto()" class="avatar-img">\n              </ion-avatar>\n            </ion-col>\n\n          </ion-row>\n          <ion-row>\n              <ion-col col-12>\n                <ion-input type="text" formControlName="first_name"  name="first_name" placeholder="First Name" required></ion-input>\n              </ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col col-12>\n                <ion-input type="text" formControlName="last_name" name="last_name" placeholder="Last Name" required></ion-input>\n              </ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col col-12>\n                <ion-select formControlName="work_type" name="work_type" placeholder="Work type" interface="action-sheet" required>\n                  <ion-option value="1">Cement Worker</ion-option>\n                  <ion-option value="2">Construction Carpenter</ion-option>\n                  <ion-option value="3">Construction Electrician</ion-option>\n                  <ion-option value="4">Dry wall</ion-option>\n                  <ion-option value="5">Floor installer</ion-option>\n                  <ion-option value="6">Framer</ion-option>\n                  <ion-option value="7">General labour</ion-option>\n                  <ion-option value="8">Glazier</ion-option>\n                  <ion-option value="9">Mason</ion-option>\n                  <ion-option value="10">Plasterer</ion-option>\n                  <ion-option value="11">Plumber</ion-option>\n                  <ion-option value="12">Welder</ion-option> \n                </ion-select>        \n              </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-2>\n                <ion-badge color="secondary" class="workapp-badge"><ion-icon name="md-mail"></ion-icon></ion-badge>\n            </ion-col>\n            <ion-col col-10>\n              <ion-input type="text" formControlName="user" name="user" placeholder="Email" required readonly></ion-input>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col col-12>\n                <ion-input type="text" formControlName="phone" name="phone" placeholder="Phone" required></ion-input>\n              </ion-col>\n          </ion-row>\n          <ion-row>\n\n            <ion-item col-12>\n                <ion-select formControlName="cities" multiple="true" placeholder="Cities" col-12>\n                  <ion-option value="1">Abbotsford</ion-option>\n                  <ion-option value="2">Aldergrove</ion-option>\n                  <ion-option value="3">Burnaby</ion-option>\n                  <ion-option value="4">Chilliwack</ion-option>\n                  <ion-option value="5">Coquitlam</ion-option>\n                  <ion-option value="6">Surrey</ion-option>\n                  <ion-option value="7">Maple Ridge</ion-option>\n                  <ion-option value="8">Mission</ion-option>\n                  <ion-option value="9">New Westminster</ion-option>\n                  <ion-option value="10">North Vancouver</ion-option>\n                  <ion-option value="11">White Rock</ion-option>\n                  <ion-option value="12">Vancouver</ion-option>\n                </ion-select>\n            </ion-item>\n          </ion-row>     \n          <ion-row>\n            <ion-col col-12>\n              <ion-textarea  formControlName="description" name="description" placeholder=" Personal description"  required></ion-textarea>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-12>\n              <ion-textarea  formControlName="experience" name="experience" placeholder=" Laboral Experience" tabindex="2" required></ion-textarea>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button outline class="btn-registro" type="submit"  >Update info</button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </form>\n      \n</ion-content>\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/config/config.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_crop__["a" /* Crop */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_6__providers__["f" /* User */]])
    ], ConfigPage);
    return ConfigPage;
}());

//# sourceMappingURL=config.js.map

/***/ })

});
//# sourceMappingURL=15.js.map