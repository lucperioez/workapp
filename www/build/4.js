webpackJsonp([4],{

/***/ 716:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SingupEmployeePageModule", function() { return SingupEmployeePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__singup_employee__ = __webpack_require__(733);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SingupEmployeePageModule = /** @class */ (function () {
    function SingupEmployeePageModule() {
    }
    SingupEmployeePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__singup_employee__["a" /* SingupEmployeePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__singup_employee__["a" /* SingupEmployeePage */]),
            ],
        })
    ], SingupEmployeePageModule);
    return SingupEmployeePageModule;
}());

//# sourceMappingURL=singup-employee.module.js.map

/***/ }),

/***/ 733:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SingupEmployeePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_image_picker__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the SingupEmployeePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SingupEmployeePage = /** @class */ (function () {
    function SingupEmployeePage(loadingCtrl, navCtrl, navParams, camera, imagePicker, file, formBuilder, user, storage) {
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.file = file;
        this.formBuilder = formBuilder;
        this.user = user;
        this.storage = storage;
        this.authForm = this.formBuilder.group({
            first_name: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required],
            last_name: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required],
            work_type: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required],
            user: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required],
            confirmPassword: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        }, { validator: this.matchingPasswords('password', 'confirmPassword') });
        //this.imgPreview = 'assets/imgs/logo.png';
    }
    SingupEmployeePage.prototype.ionViewDidLoad = function () {
    };
    SingupEmployeePage.prototype.getPhoto = function () {
        var _this = this;
        var options = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 100,
            encodingType: this.camera.EncodingType.PNG,
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.imgPreview = base64Image;
        }, function (err) {
            // Handle error
        });
    };
    SingupEmployeePage.prototype.matchingPasswords = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        };
    };
    SingupEmployeePage.prototype.singUp = function () {
        var _this = this;
        var account_info = this.authForm.value;
        account_info.role_id = 3;
        this.user.first_signup_employee(this.authForm.value).subscribe(function (resp) {
            _this.storage.set('usr', { user: resp['user'], user_detail: resp['user_detail'] });
            var loading = _this.loadingCtrl.create({
                spinner: 'hide',
                content: "<p><img src=\"assets/img/loader.gif\" width=\"50px\" padding-horizontal /><br>\n                    Registrando datos espere un momento porfavor ...</p>",
                duration: 5000
            });
            loading.onDidDismiss(function () {
                _this.navCtrl.setRoot('ConfigPage');
            });
            loading.present();
        }, function (err) {
        });
    };
    SingupEmployeePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-singup-employee',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/singup-employee/singup-employee.html"*/'\n<ion-header>\n\n    <ion-navbar>\n      <ion-title>EMPLOYEE</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n\n<ion-content class="workapp-content-register">\n    <form (ngSubmit)="singUp()" [formGroup]="authForm" novalidate>\n      <ion-grid class="register-content">\n\n          <ion-row>\n              <ion-col col-12>\n                  <ion-label>Register Employee</ion-label>\n              </ion-col>\n              <ion-col col-12>\n                <ion-input type="text" formControlName="first_name"  name="first_name" placeholder="First Name" required></ion-input>\n              </ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col col-12>\n                <ion-input type="text" formControlName="last_name" name="last_name" placeholder="Last Name" required></ion-input>\n              </ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col col-12>\n                <ion-select formControlName="work_type" name="work_type" placeholder="Work type" interface="action-sheet" required>\n                  <ion-option value="1">Cement Worker</ion-option>\n                  <ion-option value="2">Construction Carpenter</ion-option>\n                  <ion-option value="3">Construction Electrician</ion-option>\n                  <ion-option value="4">Dry wall</ion-option>\n                  <ion-option value="5">Floor installer</ion-option>\n                  <ion-option value="6">Framer</ion-option>\n                  <ion-option value="7">General labour</ion-option>\n                  <ion-option value="8">Glazier</ion-option>\n                  <ion-option value="9">Mason</ion-option>\n                  <ion-option value="10">Plasterer</ion-option>\n                  <ion-option value="11">Plumber</ion-option>\n                  <ion-option value="12">Welder</ion-option> \n                </ion-select>        \n              </ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col col-2>\n                  <ion-badge color="secondary" class="workapp-badge"><ion-icon name="md-mail"></ion-icon></ion-badge>\n              </ion-col>\n              <ion-col col-10>\n                <ion-input type="text" formControlName="user" name="user" placeholder="Email" required></ion-input>\n              </ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col col-2>\n                <ion-badge color="secondary" class="workapp-badge"><ion-icon name="md-lock"></ion-icon></ion-badge>\n                \n              </ion-col>\n              <ion-col col-10>\n                <ion-input type="password" formControlName="password" name="password" placeholder="Password" required></ion-input>\n              </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-2>\n              <ion-badge color="secondary" class="workapp-badge"><ion-icon name="md-lock"></ion-icon></ion-badge>\n              \n              </ion-col>\n              <ion-col col-10>\n                <ion-input type="confirmPassword" formControlName="confirmPassword" name="confirmPassword" placeholder="Confirm password" required></ion-input>\n              </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button outline class="btn-registro" type="submit" [disabled]="!authForm.valid" >Register</button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n    </form>\n  \n  </ion-content>\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/singup-employee/singup-employee.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_6__providers__["f" /* User */], __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */]])
    ], SingupEmployeePage);
    return SingupEmployeePage;
}());

//# sourceMappingURL=singup-employee.js.map

/***/ })

});
//# sourceMappingURL=4.js.map