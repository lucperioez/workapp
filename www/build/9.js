webpackJsonp([9],{

/***/ 712:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffertsPageModule", function() { return OffertsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__offerts__ = __webpack_require__(729);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var OffertsPageModule = /** @class */ (function () {
    function OffertsPageModule() {
    }
    OffertsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__offerts__["a" /* OffertsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__offerts__["a" /* OffertsPage */]),
            ],
        })
    ], OffertsPageModule);
    return OffertsPageModule;
}());

//# sourceMappingURL=offerts.module.js.map

/***/ }),

/***/ 729:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OffertsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the OffertsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OffertsPage = /** @class */ (function () {
    function OffertsPage(events, navCtrl, navParams, offert, storage) {
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.offert = offert;
        this.storage = storage;
        events.publish('update:menu', 'My Offerts');
        this.cities = ["Abbotsford",
            "Aldergrove",
            "Burnaby",
            "Chilliwack",
            "Coquitlam",
            "Surrey",
            "Maple Ridge",
            "Mission",
            "New Westminster",
            "North Vancouver",
            "White Rock",
            "Vancouver"];
    }
    OffertsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get('usr').then(function (val) {
            _this.getSession(val);
        });
    };
    OffertsPage.prototype.getSession = function (val) {
        this.id_user = val.user.id_user;
        this.getOfferts();
    };
    OffertsPage.prototype.addOffert = function () {
        this.navCtrl.push('OffertEditPage', { type: 'add' });
    };
    OffertsPage.prototype.editOffert = function (id) {
        this.navCtrl.push('OffertEditPage', { type: 'edit', id: id });
    };
    OffertsPage.prototype.getOfferts = function () {
        var _this = this;
        console.log(this.id_user);
        this.offert.get(this.id_user).subscribe(function (resp) {
            console.log(resp['offer']);
            _this.offerts = resp['offer'];
        }, function (err) {
        });
    };
    OffertsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-offerts',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/offerts/offerts.html"*/'<!--\n  Generated template for the OffertsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n<ion-content class="workapp-content">\n \n    <ion-card *ngFor="let offert of offerts">\n        <ion-item>     \n          <ion-row>\n            <ion-col col-3>\n              <img class="avatar" src="../assets/imgs/logo_buisness.jpg">\n            </ion-col>\n            <ion-col col-9 class="avatar-name">\n                <h2>{{offert.title}}</h2>\n                <b><p>{{cities[offert.city-1]}}</p></b>\n                <p>{{offert.address}}</p>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n        <ion-card-content>\n          <ion-row>\n            <ion-col col-3>\n              <b>Start date</b>\n            </ion-col>\n            <ion-col col-9 >\n                {{offert.start_date}}\n            </ion-col>\n            <ion-col col-3>\n              <b>End date</b>\n            </ion-col>\n            <ion-col col-9 >\n                {{offert.end_date}}\n            </ion-col>\n          </ion-row>\n          \n        </ion-card-content>\n          <ion-row class="butons-container">\n            <ion-col col-6>\n              <ion-row>\n              <ion-col col-3>\n                <b>Status</b>\n              </ion-col>\n              <ion-col col-9 >\n                 <label class="active-status">Active</label> \n              </ion-col>\n            </ion-row>\n            </ion-col>\n            <ion-col col-6>\n              <button ion-button outline class="btn-detalle"   small icon-start> \n                <ion-icon  name="md-person"></ion-icon>\n                Detail\n              </button>\n              <button ion-button outline class="btn-detalle" (click)="editOffert(offert.id_offer)"  small icon-start> \n                <ion-icon  name="md-person"></ion-icon>\n                Edit\n              </button>\n            </ion-col>\n        \n          </ion-row>\n      </ion-card>\n      <ion-card>\n      <ion-card-content text-center>\n        <button ion-button class="offert-add" (click)="addOffert()"><ion-icon name="add"></ion-icon>&nbsp;Offert</button>\n      </ion-card-content>\n      </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/offerts/offerts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["d" /* Offert */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], OffertsPage);
    return OffertsPage;
}());

//# sourceMappingURL=offerts.js.map

/***/ })

});
//# sourceMappingURL=9.js.map