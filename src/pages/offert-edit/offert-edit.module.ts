import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OffertEditPage } from './offert-edit';

@NgModule({
  declarations: [
    OffertEditPage,
  ],
  imports: [
    IonicPageModule.forChild(OffertEditPage),
  ],
})
export class OffertEditPageModule {}
