import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyReferencesPage } from './my-references';

@NgModule({
  declarations: [
    MyReferencesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyReferencesPage),
  ],
})
export class MyReferencesPageModule {}
