webpackJsonp([8],{

/***/ 713:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferencesPageModule", function() { return ReferencesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__references__ = __webpack_require__(730);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ReferencesPageModule = /** @class */ (function () {
    function ReferencesPageModule() {
    }
    ReferencesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__references__["a" /* ReferencesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__references__["a" /* ReferencesPage */]),
            ],
        })
    ], ReferencesPageModule);
    return ReferencesPageModule;
}());

//# sourceMappingURL=references.module.js.map

/***/ }),

/***/ 730:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferencesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__ = __webpack_require__(367);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ReferencesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReferencesPage = /** @class */ (function () {
    function ReferencesPage(navCtrl, navParams, callNumber) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.callNumber = callNumber;
    }
    ReferencesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReferencesPage');
    };
    ReferencesPage.prototype.call = function (number) {
        setTimeout(function () {
            var tel = number;
            window.open("tel:" + tel, '_system');
        }, 100);
    };
    ReferencesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-references',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/references/references.html"*/'<!--\n  Generated template for the ReferencesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Employee References</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row class="references-container">\n    <ion-col col-4>\n      <b>Company</b>\n    </ion-col>\n    <ion-col col-8>\n      CBA CONSTRUCTIONS\n    </ion-col>\n \n    <ion-col col-6>\n        <b>Contact</b>\n      </ion-col>\n      <ion-col col-6>\n        <b>Phone</b>\n      </ion-col>\n    <ion-col col-6>\n      Jorge Robledo\n    </ion-col>\n\n    <ion-col col-6 >\n        <button (click)="call(\'493-21-4345\')" ion-button outline item-end icon-start>\n          <ion-icon name="call"></ion-icon>\n          493-21-4345\n        </button>\n       \n    </ion-col>\n\n  </ion-row>\n\n  <ion-row class="references-container">\n      <ion-col col-4>\n        <b>Company</b>\n      </ion-col>\n      <ion-col col-8>\n        CBA CONSTRUCTIONS\n      </ion-col>\n   \n      <ion-col col-6>\n          <b>Contact</b>\n        </ion-col>\n        <ion-col col-6>\n          <b>Phone</b>\n        </ion-col>\n      <ion-col col-6>\n        Jorge Robledo\n      </ion-col>\n  \n      <ion-col col-6 (click)="call(\'493-21-4345\')">\n          <button (click)="call(\'493-21-4345\')" ion-button outline item-end icon-start>\n            <ion-icon name="call"></ion-icon>\n            493-21-4345\n          </button>\n      </ion-col>\n  \n    </ion-row>\n    \n    <ion-row class="references-container">\n        <ion-col col-4>\n          <b>Company</b>\n        </ion-col>\n        <ion-col col-8>\n          CBA CONSTRUCTIONS\n        </ion-col>\n     \n        <ion-col col-6>\n            <b>Contact</b>\n          </ion-col>\n          <ion-col col-6>\n            <b>Phone</b>\n          </ion-col>\n        <ion-col col-6>\n          Jorge Robledo\n        </ion-col>\n    \n        <ion-col col-6 (click)="call(\'493-21-4345\')">\n          <button (click)="call(\'493-21-4345\')" ion-button outline item-end icon-start>\n            <ion-icon name="call"></ion-icon>\n            493-21-4345\n          </button>\n        </ion-col>\n    \n      </ion-row>\n</ion-content>\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/references/references.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__["a" /* CallNumber */]])
    ], ReferencesPage);
    return ReferencesPage;
}());

//# sourceMappingURL=references.js.map

/***/ })

});
//# sourceMappingURL=8.js.map