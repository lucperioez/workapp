webpackJsonp([13],{

/***/ 709:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login__ = __webpack_require__(726);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__login__["a" /* LoginPage */]),
                __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__login__["a" /* LoginPage */]
            ]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 726:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(91);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, events, user, toastCtrl, translateService, loadingCtrl, storage) {
        this.navCtrl = navCtrl;
        this.events = events;
        this.user = user;
        this.toastCtrl = toastCtrl;
        this.translateService = translateService;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        // The account fields for the login form.
        // If you're using the username field with or without email, make
        // sure to add it to the type
        this.account = {
            email: '',
            password: ''
        };
        var toast = this.toastCtrl.create({
            message: "Funciona",
            duration: 3000,
            position: 'top'
        });
        toast.present();
        events.publish('close:menu');
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get('usr').then(function (val) {
            if (val) {
                switch (val.user.role_id) {
                    case '2':
                        _this.navCtrl.setRoot('SearchPage');
                        break;
                    case '3':
                        _this.navCtrl.setRoot('SearchOffersPage');
                        break;
                    default:
                        break;
                }
            }
        });
    };
    LoginPage.prototype.signupBuldier = function () {
        this.navCtrl.push('SignupPage');
    };
    LoginPage.prototype.signupEmployee = function () {
        this.navCtrl.push('SingupEmployeePage');
    };
    // Attempt to login in through our User service
    LoginPage.prototype.doLogin = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: "<p><img src=\"assets/img/loader.gif\" width=\"50px\" padding-horizontal /><br>\n                  Registrando datos espere un momento porfavor ...</p>"
        });
        loading.present();
        this.user.login(this.account).subscribe(function (resp) {
            _this.storage.set('usr', { user: resp['user'], user_detail: resp['user_detail'] });
            switch (resp['user'].role_id) {
                case '2':
                    _this.navCtrl.setRoot('SearchPage');
                    break;
                case '3':
                    _this.navCtrl.setRoot('SearchOffersPage');
                    break;
                default:
                    break;
            }
            loading.dismiss();
        }, function (err) {
            loading.dismiss();
            _this.loginErrorString = err;
            // Unable to log in
            var toast = _this.toastCtrl.create({
                message: "Usuario o contraseña incorrecta",
                duration: 3000,
                position: 'top'
            });
            toast.present();
        });
    };
    LoginPage.prototype.register = function () {
        this.navCtrl.setRoot('WelcomePage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/login/login.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content scroll="false">\n  <div class="splash-bg"></div>\n  <div class="splash-info">\n    <div class="splash-logo"></div>\n    <div class="splash-intro">\n      WORKAPP\n    </div>\n  </div>\n  <div padding>\n    <form (submit)="doLogin()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-2>\n                    <ion-badge color="secondary" class="workapp-badge"><ion-icon name="md-mail"></ion-icon></ion-badge>\n                </ion-col>\n                <ion-col col-10>\n                    <ion-input  type="email" [(ngModel)]="account.email" name="email" placeholder="Email"></ion-input>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col col-2>\n                    <ion-badge color="secondary" class="workapp-badge"><ion-icon name="md-lock"></ion-icon></ion-badge>\n                </ion-col>\n                <ion-col col-10>\n                    <ion-input  type="password" [(ngModel)]="account.password" name="password" placeholder="Password"></ion-input>\n                </ion-col>\n            </ion-row>\n           \n    \n          <div padding>\n            <button ion-button  class="login" block><b>Sing in</b></button>\n          </div>\n          <div text-center>\n             <b>Or</b> \n          </div>\n          <ion-row padding>\n            <ion-col col-6>\n              <button ion-button block (click)="signupBuldier()" class="singup">BUILDER</button>\n            </ion-col>\n            <ion-col col-6>\n              <button ion-button block (click)="signupEmployee()" class="singup">EMPLOYEE</button>\n            </ion-col>          \n          </ion-row>\n    \n        </ion-grid>\n      </form>\n\n    \n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["f" /* User */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=13.js.map