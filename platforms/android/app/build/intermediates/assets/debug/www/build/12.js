webpackJsonp([12],{

/***/ 708:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyReferencesPageModule", function() { return MyReferencesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_references__ = __webpack_require__(725);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MyReferencesPageModule = /** @class */ (function () {
    function MyReferencesPageModule() {
    }
    MyReferencesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__my_references__["a" /* MyReferencesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__my_references__["a" /* MyReferencesPage */]),
            ],
        })
    ], MyReferencesPageModule);
    return MyReferencesPageModule;
}());

//# sourceMappingURL=my-references.module.js.map

/***/ }),

/***/ 725:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyReferencesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(91);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the MyReferencesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyReferencesPage = /** @class */ (function () {
    function MyReferencesPage(storage, employee, events, navCtrl, navParams, formBuilder, loadingCtrl, toastCtrl) {
        var _this = this;
        this.storage = storage;
        this.employee = employee;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.user_references = [];
        events.publish('update:menu', 'My references');
        this.authForm = this.formBuilder.group({
            company: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            full_name: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            phone: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            description: ['']
        });
        this.storage.get('usr').then(function (val) {
            _this.imprimir(val);
        });
    }
    MyReferencesPage.prototype.imprimir = function (val) {
        this.user_id = val.user.id_user;
        this.getReferences();
    };
    MyReferencesPage.prototype.getReferences = function () {
        var _this = this;
        this.employee.getReferences(this.user_id).subscribe(function (resp) {
            _this.user_references = resp['refrences'];
        }, function (err) {
        });
    };
    MyReferencesPage.prototype.addReference = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: "<p><img src=\"assets/img/loader.gif\" width=\"50px\" padding-horizontal /><br>\n                  Registrando datos espere un momento porfavor ...</p>",
            duration: 5000
        });
        loading.present();
        loading.onDidDismiss(function () {
            var toast = _this.toastCtrl.create({
                message: 'Reference created success',
                duration: 2000,
                position: 'top'
            });
            toast.present();
            _this.authForm.controls['company'].setValue("");
            _this.authForm.controls['full_name'].setValue("");
            _this.authForm.controls['phone'].setValue("");
            _this.authForm.controls['description'].setValue("");
            _this.getReferences();
        });
        this.employee.addReference(this.authForm.value, this.user_id).subscribe(function (resp) {
            loading.dismiss();
        }, function (err) {
            var toast = _this.toastCtrl.create({
                message: 'Reference created error',
                duration: 2000,
                position: 'top'
            });
            toast.present();
        });
    };
    MyReferencesPage.prototype.delete = function (id) {
        var _this = this;
        this.employee.deleteReference(id).subscribe(function (resp) {
            var toast = _this.toastCtrl.create({
                message: 'Reference deleted success',
                duration: 2000,
                position: 'top'
            });
            toast.present();
            _this.authForm.controls['company'].setValue("");
            _this.authForm.controls['full_name'].setValue("");
            _this.authForm.controls['phone'].setValue("");
            _this.authForm.controls['description'].setValue("");
            _this.getReferences();
        }, function (err) {
            var toast = _this.toastCtrl.create({
                message: 'Reference deleted error',
                duration: 2000,
                position: 'top'
            });
            toast.present();
        });
    };
    MyReferencesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-my-references',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/my-references/my-references.html"*/'\n<ion-content class="main-content workapp-content">\n  <ion-grid class="references-grid" *ngFor="let reference of user_references; let i = index">\n    <ion-row class="references-container " >\n        <ion-col class="reference-header" col-12>\n          <b>Reference {{i+1}}</b>\n        </ion-col>\n        <ion-col col-4>\n          <br><br>\n          <b>Company</b>\n        </ion-col>\n        <ion-col col-8>\n          <button ion-button color="danger" (click)="delete(reference.id_reference)" float-right icon-only>\n              <ion-icon name="trash"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-12>\n            {{reference.company}}\n        </ion-col>\n        <ion-col col-12 class="description-container">   \n          <b>Description</b>\n          <ion-textarea [(ngModel)]="reference.description" readonly >\n            \n          </ion-textarea>\n        </ion-col>\n        <ion-col col-6>\n            <b>Contact</b>\n          </ion-col>\n          <ion-col col-6>\n            <b>Phone</b>\n          </ion-col>\n        <ion-col col-6>\n            {{reference.full_name}}\n        </ion-col>\n    \n        <ion-col col-6 >\n            <button (click)="call(reference.phone)" ion-button outline item-end icon-start>\n              <ion-icon name="call"></ion-icon>\n              {{reference.phone}}\n            </button>\n           \n        </ion-col>\n\n       \n       \n      </ion-row >\n      </ion-grid>\n      <ion-grid class="references-grid">\n            <form *ngIf="user_references.length < 3" (ngSubmit)="addReference()" [formGroup]="authForm" novalidate>\n                <ion-row class="references-container">\n              <ion-col col-3>\n                <b>Company</b>\n              </ion-col>\n              <ion-col col-9>\n                  <ion-input formControlName="company" name="company" type="text" placeholder="Company" required></ion-input>\n              </ion-col>\n          \n              <ion-col col-3>\n                <b>Contact</b>\n              </ion-col>\n              <ion-col col-9>\n                  <ion-input type="text" formControlName="full_name" name="full_name" placeholder="Contact Name" required></ion-input>\n              </ion-col>\n              <ion-col col-3>\n                <b>Phone</b>\n              </ion-col>\n              <ion-col col-9 >\n                  <ion-input type="text" formControlName="phone" name="phone" placeholder="Phone Number" required></ion-input>\n              </ion-col>\n              <ion-col col-3>\n                <b>Description</b>\n              </ion-col>\n              <ion-col col-9>\n                  <ion-textarea formControlName="description"  name="description" placeholder="Reference description" ></ion-textarea>\n              </ion-col>\n              <ion-col col-12 >\n                <button ion-button type="submit" float-right	[disabled]="!authForm.valid" >Add reference</button>\n              </ion-col>\n            </ion-row>\n            </form>\n          \n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/my-references/my-references.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["c" /* Employee */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */]])
    ], MyReferencesPage);
    return MyReferencesPage;
}());

//# sourceMappingURL=my-references.js.map

/***/ })

});
//# sourceMappingURL=12.js.map