webpackJsonp([1],{

/***/ 719:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VacantPageModule", function() { return VacantPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__vacant__ = __webpack_require__(736);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var VacantPageModule = /** @class */ (function () {
    function VacantPageModule() {
    }
    VacantPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__vacant__["a" /* VacantPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__vacant__["a" /* VacantPage */]),
            ],
        })
    ], VacantPageModule);
    return VacantPageModule;
}());

//# sourceMappingURL=vacant.module.js.map

/***/ }),

/***/ 736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VacantPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the VacantPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VacantPage = /** @class */ (function () {
    function VacantPage(navCtrl, navParams, viewCtrl, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.authForm = this.formBuilder.group({
            work_type: ['',],
            number: ['',]
        });
    }
    VacantPage.prototype.ionViewDidLoad = function () {
        console.log(this.navParams.get('id'));
    };
    VacantPage.prototype.save = function () {
        var vacant_data = this.authForm.value;
        vacant_data.status = 1;
        vacant_data.offer_id = this.navParams.get('id');
        this.viewCtrl.dismiss(vacant_data);
    };
    VacantPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-vacant',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/vacant/vacant.html"*/'<!--\n  Generated template for the VacantPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>vacant</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <form (ngSubmit)="singUp()" [formGroup]="authForm" novalidate>\n    <ion-grid class="register-content">\n       \n        <ion-row>\n            <ion-col col-6>\n              <ion-select formControlName="work_type" name="work_type" placeholder="Work type" interface="action-sheet" required>\n                <ion-option value="1">Cement Worker</ion-option>\n                <ion-option value="2">Construction Carpenter</ion-option>\n                <ion-option value="3">Construction Electrician</ion-option>\n                <ion-option value="4">Dry wall</ion-option>\n                <ion-option value="5">Floor installer</ion-option>\n                <ion-option value="6">Framer</ion-option>\n                <ion-option value="7">General labour</ion-option>\n                <ion-option value="8">Glazier</ion-option>\n                <ion-option value="9">Mason</ion-option>\n                <ion-option value="10">Plasterer</ion-option>\n                <ion-option value="11">Plumber</ion-option>\n                <ion-option value="12">Welder</ion-option> \n              </ion-select>        \n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col col-6>\n              <ion-input type="text" formControlName="number" name="number" placeholder="Number" required></ion-input>\n            </ion-col>\n        </ion-row>\n      </ion-grid>\n    </form>\n  <button ion-button class="offert-add" (click)="save()"><ion-icon name="add"></ion-icon>&nbsp;Save</button>\n</ion-content>\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/vacant/vacant.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], VacantPage);
    return VacantPage;
}());

//# sourceMappingURL=vacant.js.map

/***/ })

});
//# sourceMappingURL=1.js.map