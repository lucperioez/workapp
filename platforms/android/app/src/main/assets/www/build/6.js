webpackJsonp([6],{

/***/ 714:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search__ = __webpack_require__(731);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SearchPageModule = /** @class */ (function () {
    function SearchPageModule() {
    }
    SearchPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__search__["a" /* SearchPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__search__["a" /* SearchPage */]),
                __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__search__["a" /* SearchPage */]
            ]
        })
    ], SearchPageModule);
    return SearchPageModule;
}());

//# sourceMappingURL=search.module.js.map

/***/ }),

/***/ 731:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SearchPage = /** @class */ (function () {
    function SearchPage(events, navCtrl) {
        this.events = events;
        this.navCtrl = navCtrl;
        events.publish('update:menu', 'Find Employees');
    }
    SearchPage.prototype.clear = function () {
        this.user_cities = "";
        this.work_type = "";
        this.start_date = "";
        this.end_date = "";
        this.start_hour = "";
        this.end_hour = "";
    };
    SearchPage.prototype.search = function () {
        var search_object = {
            "cities": this.user_cities,
            "work_type": this.work_type,
            "start_date": this.start_date,
            "end_date": this.end_date,
            "start_hour": this.start_hour,
            "end_hour": this.end_hour
        };
        console.log(search_object);
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/search/search.html"*/'<ion-content class="main-content workapp-content">\n  \n  \n  <ion-grid class="search-grid">\n      <ion-row>\n        <ion-col col-12>\n            <ion-label class="search-title">WHAT ARE YOU LOOKING FOR?</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col col-12>\n            <ion-label float-left class="search-subtitle" color="primary" stacked>WORK TYPE</ion-label>\n            <ion-select [(ngModel)]="work_type" name="work_type" placeholder="Work type" multiple="true" col-12>\n              <ion-option value="1">Cement Worker</ion-option>\n              <ion-option value="2">Construction Carpenter</ion-option>\n              <ion-option value="3">Construction Electrician</ion-option>\n              <ion-option value="4">Dry wall</ion-option>\n              <ion-option value="5">Floor installer</ion-option>\n              <ion-option value="6">Framer</ion-option>\n              <ion-option value="7">General labour</ion-option>\n              <ion-option value="8">Glazier</ion-option>\n              <ion-option value="9">Mason</ion-option>\n              <ion-option value="10">Plasterer</ion-option>\n              <ion-option value="11">Plumber</ion-option>\n              <ion-option value="12">Welder</ion-option> \n            </ion-select>        \n          </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col col-12>\n            <ion-label float-left class="search-subtitle" color="primary" stacked>CITIES</ion-label>\n            <ion-select [(ngModel)]="user_cities" multiple="true" placeholder="Cities" col-12>\n                <ion-option value="1">Abbotsford</ion-option>\n                <ion-option value="2">Aldergrove</ion-option>\n                <ion-option value="3">Burnaby</ion-option>\n                <ion-option value="4">Chilliwack</ion-option>\n                <ion-option value="5">Coquitlam</ion-option>\n                <ion-option value="6">Surrey</ion-option>\n                <ion-option value="7">Maple Ridge</ion-option>\n                <ion-option value="8">Mission</ion-option>\n                <ion-option value="9">New Westminster</ion-option>\n                <ion-option value="10">North Vancouver</ion-option>\n                <ion-option value="11">White Rock</ion-option>\n                <ion-option value="12">Vancouver</ion-option>\n              </ion-select>\n          </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col col-12>\n            <ion-label float-left class="search-subtitle" color="primary" stacked >DATE REQUIRED</ion-label >\n          </ion-col>\n          <ion-col col-5>\n              <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="start_date"  name="start_date" placeholder="Start date"></ion-datetime>\n          </ion-col>\n          <ion-col col-2>\n            <ion-label text-center class="search-subtitle-to" color="primary" stacked >to</ion-label >\n          </ion-col>\n          <ion-col col-5>\n              <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="end_date"  name="end_date" placeholder="End date"></ion-datetime>\n          </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col col-12>\n            <ion-label float-left class="search-subtitle" color="primary" stacked >REQUIRED SCHEDULE</ion-label >\n          </ion-col>\n          <ion-col col-5>\n              <ion-datetime displayFormat="hh:mm A" [(ngModel)]="start_hour"  name="start_hour" placeholder="Start hour"></ion-datetime>\n          </ion-col>\n          <ion-col col-2>\n            <ion-label text-center class="search-subtitle-to" color="primary" stacked >to</ion-label >\n          </ion-col>\n          <ion-col col-5>\n              <ion-datetime displayFormat="hh:mm A" [(ngModel)]="end_hour"  name="start_hour" placeholder="Start hour"></ion-datetime>\n          </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12>\n           \n            <button ion-button icon-end (click)="search()" float-right>\n              Search\n              <ion-icon name="search"></ion-icon>\n            </button>\n            <button ion-button color="danger" icon-end (click)="clear()" float-right>\n              Clear\n              <ion-icon name="trash"></ion-icon>\n            </button>\n        </ion-col>\n      </ion-row>\n  </ion-grid>\n  \n  \n  <ion-list>\n    <button ion-item (click)="openItem(item)" *ngFor="let item of currentItems">\n      <ion-avatar item-start>\n        <img [src]="item.profilePic" />\n      </ion-avatar>\n      <h2>{{item.name}}</h2>\n      <p>{{item.about}}</p>\n      <ion-note item-end *ngIf="item.note">{{item.note}}</ion-note>\n    </button>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/search/search.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ })

});
//# sourceMappingURL=6.js.map