webpackJsonp([10],{

/***/ 711:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffertEditPageModule", function() { return OffertEditPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__offert_edit__ = __webpack_require__(728);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var OffertEditPageModule = /** @class */ (function () {
    function OffertEditPageModule() {
    }
    OffertEditPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__offert_edit__["a" /* OffertEditPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__offert_edit__["a" /* OffertEditPage */]),
            ],
        })
    ], OffertEditPageModule);
    return OffertEditPageModule;
}());

//# sourceMappingURL=offert-edit.module.js.map

/***/ }),

/***/ 728:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OffertEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the OffertEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OffertEditPage = /** @class */ (function () {
    function OffertEditPage(events, navCtrl, navParams, formBuilder, offert, loadingCtrl, storage, toastCtrl, modalCtrl) {
        var _this = this;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.offert = offert;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.Title = 'Add Offert';
        this.authForm = this.formBuilder.group({
            city: ['',],
            address: ['',],
            hour_price: ['',],
            start_date: ['',],
            end_date: ['',],
            start_hour: ['',],
            end_hour: ['',],
            description: ['',],
            title: ['',],
        });
        events.publish('update:menu', this.Title);
        this.storage.get('usr').then(function (val) {
            _this.getSession(val);
        });
    }
    OffertEditPage.prototype.getSession = function (val) {
        this.id_user = val.user.id_user;
    };
    OffertEditPage.prototype.ionViewDidLoad = function () {
        if (this.navParams.get('type') == 'edit') {
            this.getOffert(this.navParams.get('id'));
        }
    };
    OffertEditPage.prototype.getOffert = function (id) {
        var _this = this;
        this.offert.getOffert(id).subscribe(function (resp) {
            console.log(resp);
            var offer_data = resp['offer'];
            _this.authForm.controls['city'].setValue(offer_data.city);
            _this.authForm.controls['address'].setValue(offer_data.address);
            _this.authForm.controls['hour_price'].setValue(offer_data.hour_price);
            _this.authForm.controls['start_date'].setValue(offer_data.start_date);
            _this.authForm.controls['end_date'].setValue(offer_data.end_date);
            _this.authForm.controls['start_hour'].setValue(offer_data.start_hour);
            _this.authForm.controls['end_hour'].setValue(offer_data.end_hour);
            _this.authForm.controls['description'].setValue(offer_data.description);
            _this.authForm.controls['title'].setValue(offer_data.title);
        }, function (err) {
        });
    };
    OffertEditPage.prototype.save = function () {
        var _this = this;
        var offer_data = this.authForm.value;
        if (this.navParams.get('type') == 'edit') {
            offer_data.builder_id = this.id_user;
            console.log(offer_data);
            var loading_1 = this.loadingCtrl.create({
                spinner: 'hide',
                content: "<p><img src=\"assets/img/loader.gif\" width=\"50px\" padding-horizontal /><br>\n                    Registrando datos espere un momento porfavor ...</p>",
                duration: 5000
            });
            loading_1.onDidDismiss(function () {
                _this.getOffert(_this.navParams.get('id'));
                var toast = _this.toastCtrl.create({
                    message: 'Offer update success',
                    duration: 3000,
                    position: 'top'
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                });
                toast.present();
            });
            loading_1.present();
            this.offert.update(this.navParams.get('id'), offer_data).subscribe(function (resp) {
                loading_1.dismiss();
            }, function (err) {
            });
        }
        else {
            offer_data.builder_id = this.id_user;
            console.log(offer_data);
            var loading_2 = this.loadingCtrl.create({
                spinner: 'hide',
                content: "<p><img src=\"assets/img/loader.gif\" width=\"50px\" padding-horizontal /><br>\n                    Registrando datos espere un momento porfavor ...</p>",
                duration: 5000
            });
            loading_2.onDidDismiss(function () {
                _this.navCtrl.push('OffertsPage');
            });
            loading_2.present();
            this.offert.add(offer_data).subscribe(function (resp) {
                loading_2.dismiss();
            }, function (err) {
            });
        }
    };
    OffertEditPage.prototype.addVacant = function () {
        var profileModal = this.modalCtrl.create('VacantPage', { id: this.navParams.get('id') });
        profileModal.onDidDismiss(function (data) {
            console.log(data);
        });
        profileModal.present();
    };
    OffertEditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-offert-edit',template:/*ion-inline-start:"/Users/lucperio/Developer/canadiense/workapp/src/pages/offert-edit/offert-edit.html"*/'\n<ion-content class="workapp-content" >\n  <ion-card>\n  <form  [formGroup]="authForm" novalidate>\n      <ion-grid class="register-content">\n        <ion-row>\n          <ion-col col-12>\n            <ion-label color="primary" stacked>Title</ion-label>\n            <ion-input type="text" formControlName="title"  name="title" placeholder="title" required></ion-input>\n          </ion-col>\n        </ion-row>        \n        <ion-row>\n          <ion-col col-12>\n            <ion-label color="primary" stacked>Offert description</ion-label>\n            <ion-textarea formControlName="description"  name="description" placeholder="Offert description"></ion-textarea>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-item col-12>\n              <ion-label color="primary" fixed>City</ion-label>\n              <ion-select formControlName="city"  placeholder="Cities" col-8>\n                <ion-option value="1">Abbotsford</ion-option>\n                <ion-option value="2">Aldergrove</ion-option>\n                <ion-option value="3">Burnaby</ion-option>\n                <ion-option value="4">Chilliwack</ion-option>\n                <ion-option value="5">Coquitlam</ion-option>\n                <ion-option value="6">Surrey</ion-option>\n                <ion-option value="7">Maple Ridge</ion-option>\n                <ion-option value="8">Mission</ion-option>\n                <ion-option value="9">New Westminster</ion-option>\n                <ion-option value="10">North Vancouver</ion-option>\n                <ion-option value="11">White Rock</ion-option>\n                <ion-option value="12">Vancouver</ion-option>\n              </ion-select>\n          </ion-item>\n        </ion-row> \n        <ion-row>\n          <ion-col col-12>\n            <ion-label color="primary" stacked>Address</ion-label>\n            <ion-input type="text" formControlName="address"  name="address" placeholder="Address" required></ion-input>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-12>\n            <ion-label color="primary" stacked>Price per hour</ion-label>\n            <ion-input type="number" step="0.001" formControlName="hour_price"  name="hour_price" placeholder="Price per hour" required></ion-input>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n              <ion-label color="primary" stacked>Start date</ion-label>\n              <ion-datetime displayFormat="DD/MM/YYYY" formControlName="start_date"  name="start_date" placeholder="Start date"></ion-datetime>\n          </ion-col>\n          <ion-col col-6>\n              <ion-label color="primary" stacked>End date</ion-label>\n              <ion-datetime displayFormat="DD/MM/YYYY" formControlName="end_date"  name="end_date" placeholder="End date"></ion-datetime>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n              <ion-label color="primary" stacked>Start hour</ion-label>\n              <ion-datetime displayFormat="hh:mm A" formControlName="start_hour"  name="start_hour" placeholder="Start hour"></ion-datetime>\n          </ion-col>\n          <ion-col col-6>\n              <ion-label color="primary" stacked>End hour</ion-label>\n              <ion-datetime displayFormat="hh:mm A" formControlName="end_hour"  name="end_hour" placeholder="End hour"></ion-datetime>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-3>\n            <ion-label color="primary" stacked>Offer vacants</ion-label>\n            <button ion-button  class="btn-registro" type="submit" (click)="addVacant()" >addVacant </button>\n          </ion-col>\n          <ion-col col-3>\n          </ion-col>\n          <ion-col col-6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-3>\n            <button ion-button  class="btn-registro" type="submit" (click)="save()" >Save </button>\n          </ion-col>\n          <ion-col col-3>\n            <button ion-button color="secondary" class="btn-registro" type="submit"  >Publish </button>\n          </ion-col>\n          \n          <ion-col col-6>\n            <button ion-button color="yellow"  class="btn-registro" type="submit"  >Publish to team</button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </form>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/lucperio/Developer/canadiense/workapp/src/pages/offert-edit/offert-edit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["d" /* Offert */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], OffertEditPage);
    return OffertEditPage;
}());

//# sourceMappingURL=offert-edit.js.map

/***/ })

});
//# sourceMappingURL=10.js.map