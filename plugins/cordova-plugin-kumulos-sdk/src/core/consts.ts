export const SdkInfo = {
    id: 6,
    version: '3.1.3'
};

export const CordovaRuntimeType = 3;

export const NativeModuleName = 'KumulosSDKPlugin';

export const KumulosEvent = {
    AppForegrounded: 'k.fg',
    PushTrackOpen: 'k.push.opened',
    EngageBeaconEnteredProximity: 'k.engage.beaconEnteredProximity',
    EngageLocationUpdated: 'k.engage.locationUpdated',
    CrashLoggedException: 'k.crash.loggedException'
};

export const BeaconType = {
    iBeacon: 1,
    Eddystone: 2
};

export const CrashReportFormat = 'raven';
