export declare const SdkInfo: {
    id: number;
    version: string;
};
export declare const CordovaRuntimeType = 3;
export declare const NativeModuleName = "KumulosSDKPlugin";
export declare const KumulosEvent: {
    AppForegrounded: string;
    PushTrackOpen: string;
    EngageBeaconEnteredProximity: string;
    EngageLocationUpdated: string;
    CrashLoggedException: string;
};
export declare const BeaconType: {
    iBeacon: number;
    Eddystone: number;
};
export declare const CrashReportFormat = "raven";
